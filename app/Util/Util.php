<?php
namespace App\Util;

class Util
{

    public static function simple_curl($type = "POST", $url, $parameters = NULL, $json = true) 
    {
        if($type == "GET" && count($parameters)==0)
        {
            $parameters = NULL;
        }
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($type));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if(!is_null($parameters))
        {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($parameters));            
        }
        $headers = [
            "Content-Type: application/json",
            "charset: UTF-8",
        ];
        if(!$json)
        {
        	$headers = [
	            "Content-Type: text/html",
	            "x-requested-with: XMLHttpRequest",
        	];
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        if($json)
        {
        	$return['data'] = json_decode(curl_exec($ch));
        }
        else
        {
        	$return['data'] = curl_exec($ch);
        }

        $return['code_http'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return $return;
    }

}