<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filter;

class FilterController extends Controller
{
    public function index()
    {
        return Filter::getFilters();
    }

    public function show($type)
    {
        return Filter::getFilters($type);
    }
    
}
