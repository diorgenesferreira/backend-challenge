<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Util\Util;
use Wa72\HtmlPageDom;
include_once('../vendor/simple-html-dom/simple-html-dom/simple_html_dom.php');

class Vehicle extends Model
{
    
   public static $fillable_attrs = [
    						"tipoVeiculo"=>'',
							"marca"=>'',
							"modelo"=>'',
							"versao"=>'',
							"anoDe"=>'',
							"anoAte"=>'',
							"precoDe"=>'',
							"precoAte"=>'',
							"kmDe"=>'',
							"kmAte"=>'',
							"motor"=>'',
							"combustivel"=>'',
							"cor"=>'',
							"portas"=>'',
							"type"=>'', 
							"modelo"=>'',
							"cidades"=>'',
    					  ];


	public static $URL_PROVIDER = "https://seminovos.com.br/{type/}{marca/}{modelo/}{cidades/}?ajax";

	public static function getTipoVeiculo($type)
	{
		switch (strtoupper($type)) {
			case 'CARRO':
				return 1;
				break;
			
			case 'CAMINHAO':
				return 2;
				break;

			case 'MOTO':
				return 3;
				break;
		}
	}

	public static function prepareUrl($attributes = [])
	{
		$attr_set = self::$fillable_attrs;
		$new_url = self::$URL_PROVIDER;
		if(in_array("type", array_keys($attributes)))
		{
			$new_url = str_replace("{type/}", $attributes["type"]."/", $new_url);
			$attr_set["tipoVeiculo"] = self::getTipoVeiculo($attributes["type"]);
		}
		else
		{
			$new_url = str_replace("{type/}", "", $new_url);
		}
		unset($attributes['type']);

		if(in_array("marca", array_keys($attributes)))
		{
			$new_url = str_replace("{marca/}", $attributes["marca"]."/", $new_url);
		}
		else
		{
			$new_url = str_replace("{marca/}", "", $new_url);
		}
		unset($attributes['marca']);

		if(in_array("modelo", array_keys($attributes)))
		{
			$new_url = str_replace("{modelo/}", $attributes["modelo"]."/", $new_url);
		}
		else
		{
			$new_url = str_replace("{modelo/}", "", $new_url);
		}
		unset($attributes['modelo']);

		if(in_array("cidades", array_keys($attributes)))
		{
			$new_url = str_replace("{cidades/}", "cidade-[]-".implode("-", $attributes["cidades"])."/", $new_url);
		}
		else
		{
			$new_url = str_replace("{cidades/}", "", $new_url);
		}
		unset($attributes['cidades']);
		$new_url = str_replace("/?ajax", "?ajax", $new_url);
		$new_url .= "&".http_build_query($attr_set);
		return $new_url;
	}

	public static function getAcessorios($dom_items)
	{
		$acessorios = [];

		foreach ($dom_items as $item) 
		{
			$acessorios[] = trim(str_replace("&bull;", "", $item->plaintext));
		}
		return $acessorios;
	}

	public static function filter($attributes = [])
	{
		//dd($attributes);
		foreach ($attributes as $key => $value) 
		{
			$invalid_attrs = [];
			if(!in_array($key, array_keys(self::$fillable_attrs)))
			{
				$invalid_attrs[] = $key;
			}
			if(count($invalid_attrs)>0)
			{
				return response()->json(["error"=>"Invalid attributes (".implode(",", $invalid_attrs).")"], 400);
			}
		}

		$url = self::prepareUrl($attributes);

		$data = Util::simple_curl("get", $url, NULL, false)['data'];
		$doc = str_get_html($data);
		$itens = $doc->find('.anuncio-container');
		$vehicles = [];
		foreach ($itens as $item) 
		{
			if(isset($item->find("img")[0]))
			{
				$vehicle 					= self::createVehicle();
				$vehicle->img 				= $item->find("img")[0]->attr['src'];
				$vehicle->url 				= trim($item->find(".header a")[0]->attr['href']);
				$vehicle->title 			= trim($item->find(".title h4")[0]->plaintext);
				$vehicle->description 		= trim($item->find(".description b")[0]->plaintext);
				$vehicle->ano 				= trim($item->find(".ano span")[0]->plaintext);
				$vehicle->kilometragem 		= trim($item->find(".kilometragem span")[0]->plaintext);
				$vehicle->combustivel 		= trim($item->find(".combustivel span")[0]->plaintext);
				$vehicle->cambio 			= trim($item->find(".cambio span")[0]->plaintext);
				$vehicle->acessorios 		= self::getAcessorios($item->find(".card-acessorios .acessorio")); 
				$vehicle->valor 			= trim($item->find(".value a")[0]->plaintext);
				$vehicle->localizacao 		= trim($item->find(".localizacao span")[0]->plaintext);
				$vehicles[] 				= $vehicle;
			}
		}
		$num_pages = $doc->find('.pagination-container b')[1]->plaintext;
		return response()->json(["vehicles"=>$vehicles, "num_pages"=>$num_pages], 200);
	}

	public static function createVehicle()
	{
		$vehicle 					= new \stdClass;
		$vehicle->url 				= "";
		$vehicle->img 				= "";
		$vehicle->title 			= "";
		$vehicle->description 		= "";
		$vehicle->ano 				= "";
		$vehicle->kilometragem 		= "";
		$vehicle->combustivel 		= "";
		$vehicle->cambio 			= "";
		$vehicle->acessorios 		= [];
		$vehicle->valor 			= 0;
		$vehicle->localizacao 		= 0;
		return $vehicle;
	}

}
