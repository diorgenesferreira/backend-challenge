<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vehicle;

class VehicleController extends Controller
{
    public function search(Request $request)
    {
    	//dd($request->all());
        return Vehicle::filter($request->all());
    }
}
