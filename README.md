# Backend Challenge #

This repository contains the backend challenge of Diorgenes Ferreira.

### Getting Start ###

The project was developed with Laravel. To execute the project:

* Clone the repository
* Install vendors with 'composer install'
* In the project directory open the shell run the laravel serve ("php artisan serve")
* Access the project via browser or Postman ("http://127.0.0.1:8000")

### Extra info ###

The project has a quick documentation that can be accessed by the link http://127.0.0.1:8000