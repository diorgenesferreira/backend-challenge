<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Util\Util;

class Filter extends Model
{
	public static $URL_PROVIDER = "https://seminovos.com.br/filtros";

	public static function getData()
	{
		return Util::simple_curl("get", self::$URL_PROVIDER);
	}

    public static function getFilters($type = null, $id = null)
    {
    	$data = self::getData()['data'];
    	if(!is_null($type))
    	{
    		foreach ($data as $item) 
    		{
    			if(strtoupper($item->nome)==strtoupper($type))
    			{
    				return response()->json($item, 200);
    			}
    		}
    		return response()->json(["error"=>"Type not found"], 400);
    	}
    	if(!is_null($id))
    	{
    		foreach ($data as $item) 
    		{
    			if($item->id==$id)
    			{
    				return response()->json($item, 200);
    			}
    		}
    		return response()->json(["error"=>"ID not found"], 400);
    	}
    	return response()->json($data, 200);
    }


}
