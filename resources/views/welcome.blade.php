<!DOCTYPE html>
<html>
<head>
    <title>Backend Challenge</title>
    <link rel="shortcut icon" href="img/favicon.png">
    <!-- CSS only -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- JS, Popper.js, and jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand logo" href="#"><img src="img/logo.png"></a>
    </nav>

    <main role="main" class="container-fluid">
        <div class="row">
            <div class="col-md-3">
                <ul class="nav flex-column" id="sidebar">
                    <li class="nav-item">
                        <a class="nav-link active" href="#introduction">Introduction</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#filters">Filters</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#vehicles">Vehicles</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#errors">Errors</a>
                    </li>
                </ul>
            </div>   
            <div class="col-md-9">
                <div id="content">
                    <section id="introduction">
                        <h2>Introduction</h2>
                        <p>This application is a RESTful Api to get vehicles information from the <a href="https://seminovos.com.br/">https://seminovos.com.br/</a>.</p>
                        <p><b>Api endpoint: </b>http://127.0.0.1:8000/api/</p>
                        <p>Which was developed for Diogenes Ferreira.</p>
                    </section>
                    <section id="filters">
                        <h2>Filters</h2>
                        <p>This endpoint allow access information about the filters that can be use to combine queries for vehicles.</p>
                        <p>There are two possible requests:</p>
                        <h4>All Filters</h4>
                        <p class="endpoint"><b>GET: </b>http://127.0.0.1:8000/api/filter</p>
                        <p>This endpoint will retrive all filter information</p>
                        <h4>Specific filter</h4>
                        <p class="endpoint"><b>GET: </b>http://127.0.0.1:8000/api/filter/{type}</p>
                        <p>This endpoint will retrive all information about a specific filter by passing the filter type.</p>
                    </section>
                    <section id="vehicles">
                        <h2>Vehicles</h2>
                        <p class="endpoint"><b>GET: </b>http://127.0.0.1:8000/api/vehicle/search</p>
                        <p>This endpoint allow access information about the the vehicles with a query.</p>
                        <p>The query can use those attributes on body request:</p>
                        <ul>
                            <li>tipoVeiculo (number)</li>
                            <li>type (name)</li>
                            <li>marca</li>
                            <li>modelo</li>
                            <li>versao</li>
                            <li>anoDe</li>
                            <li>anoAte</li>
                            <li>precoDe</li>
                            <li>precoAte</li>
                            <li>kmDe</li>
                            <li>kmAte</li>
                            <li>motor</li>
                            <li>combustivel</li>
                            <li>cor</li>
                            <li>portas</li>
                            <li>type</li>
                            <li>modelo</li>
                            <li>cidades</li>
                        </ul>
                        <h4>Request example:</h4>
                        <pre>
                            <code>
                                {{ trim('{
                                  "marca": "audi",
                                  "type": "carro"
                                }') }}
                            </code>
                        </pre>
                        <h4>Response:</h4>
                        <pre>
                            <code>
                                {{ trim('{
                                    "vehicles": [
                                        {
                                            "url": "/audi-a3-sportback-1.4-tfsi-s-tronic-1.4-16v-4portas-2015-2016--2737357",
                                            "img": "https://tcarros.seminovosbh.com.br/mini_audi/a3/2015/2016/2737357/2389e76271b6935cafc5150b491048903ad8",
                                            "title": "Audi A3",
                                            "description": "Sportback 1.4 TFSI S-Tronic",
                                            "ano": "2015/2016",
                                            "kilometragem": "63.000km",
                                            "combustivel": "Gasolina",
                                            "cambio": "Automático",
                                            "acessorios": [
                                                "ABS",
                                                "AIR BAGS 7",
                                                "ALARME",
                                                "AR CONDICIONADO",
                                                "AR QUENTE",
                                                "ASSIST. PARTIDA EM RAMPA",
                                                "BANCO AJUSTE ALTURA",
                                                "CÂMBIO AUTOMÁTICO"
                                            ],
                                            "valor": "R$ 69.900,00",
                                            "localizacao": "Belo Horizonte"
                                        },
                                        ...
                                    ],
                                    "num_pages": "14"
                                }') }}
                            </code>
                        </pre>
                    </section>
                    <section id="errors">
                        <h2>Errors</h2>
                        <p>Any erros will return a 400 http code.</p>
                    </section>
                </div>
            </div>   
        </div>
    </main>

</body>
</html>